Paper:

[Modeling Biological Immunity to Adversarial Examples (Kim et al.) (2020)](http://openaccess.thecvf.com/content_CVPR_2020/html/Kim_Modeling_Biological_Immunity_to_Adversarial_Examples_CVPR_2020_paper.html)

Parameters from:

[A model of high-frequency oscillatory potentials in retinal ganglion cells (2003)](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3348786/)

Secondary Literature:

[Inhibitory Interneurons in the Retina: Types, Circuitry, and Function](https://www.annualreviews.org/doi/full/10.1146/annurev-vision-102016-061345?url_ver=Z39.88-2003&rfr_id=ori%3Arid%3Acrossref.org&rfr_dat=cr_pub++0pubmed)

[Sparse Dictionary Learning by Dynamical Neural Networks](https://openreview.net/forum?id=B1gstsCqt7)
